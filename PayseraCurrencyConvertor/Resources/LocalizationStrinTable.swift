//
//  LocalizationStrinTable.swift
//  CashBox
//
//  Created by Armin on 9/17/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation

//MARK: - LocalizedString
extension String {
    
    static let default_language_changes_description = "default_language_changes_description".localized
    static let login = "login".localized
    static let signup = "signup".localized
    static let reset_password = "reset_password".localized
    static let verification_code = "verification_code".localized
    static let reset_password_destination = "reset_password_destination".localized
    static let eup_describtion = "eup_describtion".localized
    static let login_with = "login_with".localized
    static let forget_password = "forget_password".localized
    static let dont_have_account = "dont_have_account".localized
    static let create_account = "create_account".localized
    static let next = "next".localized
    static let password = "password".localized
    static let username_invalid = "username_invalid".localized
    static let phone_number = "phone_number".localized
    static let email_address = "email_address".localized
    static let have_account = "have_account".localized
    static let create_account_description = "create_account_description".localized
    static let search_country_description = "search_country_description".localized
    static let select_country = "select_country".localized
    static let username = "username".localized
    static let verification_code_description = "verification_code_description".localized
    static let resend_code = "resend_code".localized
    static let home = "home".localized
    static let notif = "notif".localized
    static let search = "search".localized
    static let profile = "profile".localized
    static let reset_password_description = "reset_password_description".localized
    static let reset_password_destination_description = "reset_password_destination_description".localized
    static let new_password = "new_password".localized
    static let change_password = "change_password".localized
    static let confirm_password = "confirm_password".localized
    static let change_password_description = "change_password_description".localized
    static let user_credential_description = "user_credential_description".localized
    
    static let fullname = "fullname".localized
    static let biography = "biography".localized
    static let mobile = "mobile".localized
    
    static let edit_profile = "edit_profile".localized
    static let add_people = "add_people".localized

    static let messenger = "messenger".localized
    static let chats = "chats".localized
    static let channels = "channels".localized
    static let groups = "groups".localized

}


