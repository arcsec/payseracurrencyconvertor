//
//  Config.swift
//  Created by Armin on 3/3/22.
//

import Foundation
import SwiftyPlistManager

public let commission_fee:Double = 0.007
public let zero_commision_fee_amount_treshold:Double = 200.0
public let initial_non_commision_number:Int = 5


// MARK: - SourceCurrencies
enum Currencies: String {
    case EUR = "EUR", USD = "USD",
         JPY = "JPY"
    static let all = [EUR, USD, JPY]
}

class Config: NSObject {

    let dataPlistName = "DataStore"
    
    let eurDic = ["currency": "EUR", "balance_amount": 1000.0] as [String : Any]
    let usdDic = ["currency": "USD", "balance_amount": 0.0] as [String : Any]
    let jpyDic = ["currency": "JPY", "balance_amount": 0.0] as [String : Any]

    override init() {
        super.init()
        commonInit()
    }
    
    
    private func commonInit(){
        SwiftyPlistManager.shared.start(plistNames: [dataPlistName], logging: true)
        SwiftyPlistManager.shared.addNew(eurDic, key: "\(Currencies.EUR)", toPlistWithName: dataPlistName) { (err) in}
        SwiftyPlistManager.shared.addNew(usdDic, key: "\(Currencies.USD)", toPlistWithName: dataPlistName) { (err) in}
        SwiftyPlistManager.shared.addNew(jpyDic, key: "\(Currencies.JPY)", toPlistWithName: dataPlistName) { (err) in}

        SwiftyPlistManager.shared.addNew(commission_fee, key: "Commision", toPlistWithName: dataPlistName) { (err) in}
        SwiftyPlistManager.shared.addNew(zero_commision_fee_amount_treshold, key: "ZeroCommisionTreshold", toPlistWithName: dataPlistName) { (err) in}
        SwiftyPlistManager.shared.addNew(initial_non_commision_number, key: "InitialNonCommision", toPlistWithName: dataPlistName) { (err) in}
        SwiftyPlistManager.shared.addNew(0, key: "ExchangeOrderNumber", toPlistWithName: dataPlistName) { (err) in}

        UserData.shared.storeConfigInitilized("true")

    }
}
