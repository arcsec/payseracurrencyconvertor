//
//  Exchange.swift
//
//  Created by Armin Rassadi.
//  Copyright © Armin Rassadi. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Exchange{
    
    var amount : String!
    var currency : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(_ json: JSON) {
        amount = json["amount"].stringValue
        currency = json["currency"].stringValue
    }
}

func loadData() -> Dictionary<String, Any> {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentDirectory = paths[0] as! String
    let path = documentDirectory.appending("DataStore.plist")
    let fileManager = FileManager.default
    if(!fileManager.fileExists(atPath: path)){
        if let bundlePath = Bundle.main.path(forResource: "DataStore", ofType: "plist"){
            let result = NSMutableDictionary(contentsOfFile: bundlePath)
            print("Bundle file DataStore.plist is -> \(result?.description ?? "")")
            do{
                try fileManager.copyItem(atPath: bundlePath, toPath: path)
            }catch{
                print("copy failure.")
            }
        }else{
            print("file DataStore.plist not found.")
        }
    }else{
        print("file DataStore.plist already exits at path.")
    }

    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("load DataStore.plist is ->\(resultDictionary?.description ?? "")")

    let myDict = NSDictionary(contentsOfFile: path)
    return myDict as! Dictionary<String, Any>
    
}

