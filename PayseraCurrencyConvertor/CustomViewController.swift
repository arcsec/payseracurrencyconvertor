//
//  CustomViewController.swift
//
//  Created by Armin on 10/7/20.
//  Copyright © 2020 Armin@Armin. All rights reserved.
//

import UIKit

class CustomViewController: UIViewController {

   let btnLeftMenu: UIButton = UIButton()
    let btnRightMenu: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0xffffff, a: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont.RobotoRegular.withSize(16),NSAttributedString.Key.foregroundColor: UIColor(rgb: 0xffffff, a: 1)]
        
        UIBarButtonItem.appearance()
            .setTitleTextAttributes([NSAttributedString.Key.font: UIFont.RobotoRegular.withSize(16),NSAttributedString.Key.foregroundColor: UIColor(rgb: 0xffffff, a: 1)], for: .normal)
        
        self.setupLeftButton()
        self.setupRightButton()
    }
    
    
    func setupLeftButton(){
        
        self.btnLeftMenu.setImage(UIImage(named: "arrow-left-enable"), for: .normal)
        self.btnLeftMenu.addTarget(self, action: #selector(self.leftBarButtonAction), for: .touchUpInside)
        
        self.btnLeftMenu.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let leftBarButton = UIBarButtonItem(customView: self.btnLeftMenu)
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func setupRightButton(){
        
        self.btnRightMenu.setTitle("", for: .normal)
        
        let rightBarButton = UIBarButtonItem(customView: self.btnRightMenu)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    @objc func leftBarButtonAction(){
        
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    @objc func rightBarButtonAction(){
        
        
    }
}
