//
//  ViewController.swift
//  PayseraCurrencyConvertor
//
//  Created by NGN on 4/14/22.
//

import UIKit
import SwiftyPlistManager

class ViewController: CustomViewController {
    
    let dataPlistName = "DataStore"

    @IBOutlet var exchangeTableView: UITableView!
    @IBOutlet var balanceCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.exchangeTableView.register(UINib(nibName: "CurrencyExchangeTableViewCell" , bundle:nil), forCellReuseIdentifier: "CurrencyExchangeTableViewCellID")
        self.exchangeTableView.delegate = self
        self.exchangeTableView.dataSource = self
        self.exchangeTableView.contentInset = UIEdgeInsets.zero
        self.exchangeTableView.tableFooterView = UIView()
        self.exchangeTableView.separatorColor = charcoalGrey
        
        
        self.balanceCollectionView.register(UINib(nibName: "BalanceCollectionViewCell" , bundle:nil) , forCellWithReuseIdentifier: "BalanceCollectionViewCellID")
        self.balanceCollectionView.showsHorizontalScrollIndicator = false
        
        self.balanceCollectionView.isScrollEnabled = true
        self.balanceCollectionView?.contentInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        self.balanceCollectionView.delegate = self
        self.balanceCollectionView.dataSource = self
        
        
        self.exchangeTableView.reloadData()
        self.balanceCollectionView.reloadData()
    }
}


extension ViewController:UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyExchangeTableViewCellID", for: indexPath)
        as! CurrencyExchangeTableViewCell
        
        cell.delegate = self
        cell.parentViewController = self
        cell.preservesSuperviewLayoutMargins = false;
        cell.separatorInset = UIEdgeInsets.zero;
        cell.layoutMargins = UIEdgeInsets.zero;
        cell.selectionStyle = UITableViewCell.SelectionStyle.none;
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 340
    }
}


extension ViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return Currencies.all.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BalanceCollectionViewCellID" , for: indexPath) as! BalanceCollectionViewCell
        
        guard let currencyValue:Dictionary = SwiftyPlistManager.shared.fetchValue(for: "\(Currencies.all[indexPath.row])", fromPlistWithName: self.dataPlistName) as? Dictionary<String, AnyObject>
        else {
            return cell
        }

        let currencyBalance:Double = currencyValue["balance_amount"] as! Double
        let currencyName:String = currencyValue["currency"] as! String

        let currencyBalanceStr:String = "\(currencyBalance)"

        cell.balanceLabel.text = currencyBalanceStr + "-" + currencyName
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
      return
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let itemSize = (UIScreen.main.bounds.size.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 3
        return CGSize(width: itemSize, height: itemSize)
    }
}

extension ViewController:CurrencyExchangeDelegate{
    func assetsAmountShouldUpdate() {
        self.balanceCollectionView.reloadData()
    }
}
