//
//  UserData.swift
//
//  Created by Armin on 8/22/20.
//  Copyright © 2020 Armin. All rights reserved.
//

import Foundation
import KeychainAccess

class UserData:NSObject {
    static let shared = UserData()
    var keychain:Keychain
    
    override init() {
        
        self.keychain = Keychain(service: "com.Paysera.PayseraCurrencyConvertor")
        super.init()
    }
    
    func storeConfigInitilized(_ didInit: String) {
        
        self.keychain["config_initilized"] = didInit
    }
    
    var configInitilized:String? {
        
        return self.keychain[string:"config_initilized"]
    }
  
}

