//
//  Requests.swift
//  Hamyan
//
//  Created by Armin on 9/27/18.
//  Copyright © 2018 Armin. All rights reserved.

import Foundation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

typealias PlainCompletionHandler = (_ success:Bool) -> Void
typealias APICompletionHandler = (_ success:Bool,_ obj: AnyObject?) -> Void
typealias UploadCompletionHandler = (_ uploadReq: Alamofire.UploadRequest?) -> Void
typealias DownloadCompletionHandler = (_ uploadReq: Alamofire.DownloadRequest?) -> Void


class Connectivity {
    class var isConnectedToInternet:Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
}

class Requests{
    static let shared = Requests()
    
    var sessionManager: Session = {
        
        //            let manager = ServerTrustManager(evaluators: ["serverurl.com": DisabledEvaluator()])
        let serverTrustPoliciesEvaluators: ServerTrustManager = ServerTrustManager.init(allHostsMustBeEvaluated: false, evaluators: ["" : DisabledTrustEvaluator()])
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = URLSessionConfiguration.af.default.httpAdditionalHeaders
        let manager:Session = Alamofire.Session(configuration: configuration,  serverTrustManager: serverTrustPoliciesEvaluators)
        
        return manager
    }()
    
    func request(_ router: Router, indicator:Bool=false, completion: @escaping(JSON) -> (), onError: @escaping(Error) ->()) {
        
        print(router.encoding)
        print(router.parameters as Any)
        print(router.endUrl)
        
        
        //ToDo
        if indicator {
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(type: .ballClipRotateMultiple), nil)
        }
        
        sessionManager.request(router.endUrl, method: router.method, parameters: router.parameters, encoding: router.encoding, headers: router.headers)
            .validate(statusCode : router.validate)
            .responseJSON(completionHandler: { (response) in
                
                //ToDo:Dismiss Indicator
                
                switch response.result {
                case .success(let value):
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                    let json = JSON(value)
                    completion(json)
                case .failure(let error):
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                    print(response.data)
                    print(response.value)
                    print(error)
                    onError(error)
                }
                
            })
            
    }
    
}
