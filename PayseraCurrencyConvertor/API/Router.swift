//
//  Router.swift
//
//  Created by Armin on 8/9/20.
//  Copyright © 2020 Armin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftUI

enum Router {
    
    static var baseUrlString:String
    {
        return "http://api.evp.lt/currency/commercial/exchange/"
    }
    
    private var baseUrl:URL
    {
        return URL(string: Router.baseUrlString)!
    }
    
    case exchange_currency(source_currency:String, destination_currency:String, amount:String)
   

    var endUrl:URL {
        
        switch self {
        
        case .exchange_currency(let source_currency, let destination_currency, let amount):
            return self.baseUrl.appendingPathComponent("\(amount)-\(source_currency)/\(destination_currency)/latest")
        }
    }
    
    var method: HTTPMethod {
        
        switch self {
            
        case .exchange_currency:
            return HTTPMethod.get
            
        }
    }
    
    var parameters:Parameters? {
        
        switch self {
            
        case .exchange_currency:
            return nil
  

        }
    }
    
    var encoding: ParameterEncoding {
        
        switch self {
            
        case .exchange_currency:
            return JSONEncoding.default
        }
        
    }
    
    var headers: HTTPHeaders? {
        
     
        switch self {

        case .exchange_currency:
            return ["Content-Type":"application/json"]
        }
        
    }
    
    var validate:[Int] {
        
        switch self {
            
        case .exchange_currency:
            
            return [200]
            
        }
        
    }
    
}
