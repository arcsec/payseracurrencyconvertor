//
//  TapGesture.swift
//
//  Created by Armin on 11/14/18.
//  Copyright © 2018 . All rights reserved.
//

import UIKit

class TapGesture: UITapGestureRecognizer {

    override init(target: Any?, action: Selector?) {
        super.init(target: target, action: action)
    }
    
    convenience init(targetView:UIView, targetController:UIViewController, targetFunction:Selector) {
        self.init()
        let tapGesture = UITapGestureRecognizer(target: targetController, action: targetFunction)
        tapGesture.numberOfTapsRequired = 1
        targetView.addGestureRecognizer(tapGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
