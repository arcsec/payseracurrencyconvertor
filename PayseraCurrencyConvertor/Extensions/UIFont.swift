//
//  Font.swift
//
//  Created by Armin on 9/25/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit


extension UIFont {

    static let RobotoThin = UIFont(name: "Roboto-Thin", size: UIFont.labelFontSize)!
    static let RobotoMedium = UIFont(name: "Roboto-Medium", size: UIFont.labelFontSize)!
    static let RobotoLight = UIFont(name: "Roboto-Light", size: UIFont.labelFontSize)!
    static let RobotoCondensed = UIFont(name: "Roboto-Condensed", size: UIFont.labelFontSize)!
    static let RobotoBoldCondensed = UIFont(name: "Roboto-BoldCondensed", size: UIFont.labelFontSize)!
    static let RobotoBlack = UIFont(name: "Roboto-Black", size: UIFont.labelFontSize)!
    static let RobotoRegular = UIFont(name: "Roboto-Regular", size: UIFont.labelFontSize)!
    static let RobotoBold = UIFont(name: "Roboto-Bold", size: UIFont.labelFontSize)!

    
    
    
}


class FontExtension
{
    class func getFontNames()
    {
        for family in UIFont.familyNames.sorted() {
             let names = UIFont.fontNames(forFamilyName: family)
             print("Family: \(family) Font names: \(names)")
         }
    }
}
