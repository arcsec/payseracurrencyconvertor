//
//  UILabel.swift
//
//  Created by Armin on 9/27/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func append(contentOf string: String, with color: UIColor) {
        self.text?.append(contentsOf: string)
        guard let text = self.text else {
            return
        }
        let strNumber: NSString = (text) as NSString
        let range = (strNumber).range(of: string)
        let attribute = NSMutableAttributedString(string: self.text!)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        self.attributedText = attribute
    }
    
    func prepareForMultiLine() {
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
    
}

