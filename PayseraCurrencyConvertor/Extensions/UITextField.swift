//
//  UITextField.swift
//
//  Created by Armin on 9/27/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit

extension UITextField
{
  
    func setPlaceHolderColor(_ color: UIColor)
    {
       self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    func dismissKeyboard(view:UIView) {
        
        self.resignFirstResponder()
        view.endEditing(true)
    }
}
