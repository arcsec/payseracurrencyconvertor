//
//  CustomDrop.swift
//
//  Created by Armin on 10/12/20.
//  Copyright © 2020 Armin@Armin. All rights reserved.
//

import UIKit
import SwiftyDrop

enum CustomDrop: DropStatable {
    
    case succees
    case failure
    
    var textAlignment: NSTextAlignment? {
        
        switch self {
        case .succees, .failure:
          
                return .left
            
        }
    }
    
    var backgroundColor: UIColor? {
        switch self {
        case .succees:
            return greenG40
            
        case .failure:
            return redR400
        }
    }
    var font: UIFont? {
        switch self {
        case .succees, .failure:
            return UIFont.RobotoRegular.withSize(16)
        }
    }
    var textColor: UIColor? {
        switch self {
        case .succees, .failure:
            return .white
        }
    }
    var blurEffect: UIBlurEffect? {
        switch self {
        case .succees, .failure:
            return nil
        }
    }
}
