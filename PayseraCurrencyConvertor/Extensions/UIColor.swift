//
//  UIColor.swift
//
//  Created by Armin on 9/22/20.
//  Copyright © 2020 Armin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
    
    convenience init(colorString: String) {
        let hex = colorString.trimmingCharacters(
            in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        var a: UInt32 = 255
        var r: UInt32 = 255
        var g: UInt32 = 255
        var b: UInt32 = 255
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            print("Color Extention Error")
        }
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(a) / 255)
    }
}

public let redR50:UIColor = UIColor(rgb: 0xf75286, a: 1.0)
public let redR100:UIColor = UIColor(rgb: 0xf52969, a: 1.0)
public let redR200:UIColor = UIColor(rgb: 0xee0b52, a: 1.0)
public let redR300:UIColor = UIColor(rgb: 0xca0946, a: 1.0)
public let redR400:UIColor = UIColor(rgb: 0xac083c, a: 1.0)

public let orangeO5:UIColor = UIColor(rgb: 0xffcab4, a: 1.0)
public let orangeO10:UIColor = UIColor(rgb: 0xffa782, a: 1.0)
public let orangeO20:UIColor = UIColor(rgb: 0xff8856, a: 1.0)
public let orangeO30:UIColor = UIColor(rgb: 0xff6c2f, a: 1.0)
public let orangeO40:UIColor = UIColor(rgb: 0xff5008, a: 1.0)


public let grayG900:UIColor = UIColor(rgb: 0x060b11, a: 1.0)
public let grayG800:UIColor = UIColor(rgb: 0x070d14, a: 1.0)
public let grayG700:UIColor = UIColor(rgb: 0x091018, a: 1.0)
public let grayG600:UIColor = UIColor(rgb: 0x0b131d, a: 1.0)
public let grayG500:UIColor = UIColor(rgb: 0x0d1723, a: 1.0)
public let grayG400:UIColor = UIColor(rgb: 0x0f1b29, a: 1.0)
public let grayG300:UIColor = UIColor(rgb: 0x122030, a: 1.0)
public let grayG200:UIColor = UIColor(rgb: 0x152538, a: 1.0)
public let grayG100:UIColor = UIColor(rgb: 0x192b42, a: 1.0)
public let grayG50:UIColor = UIColor(rgb: 0x1d324d, a: 1.0)



public let blueStyle:UIColor = UIColor(rgb: 0x0041fd, a: 1.0)
public let blueB300:UIColor = UIColor(rgb: 0x0a78d4, a: 1.0)
public let blueB200:UIColor = UIColor(rgb: 0x0b87ee, a: 1.0)
public let blueB100:UIColor = UIColor(rgb: 0x2094f5, a: 1.0)
public let blueB50:UIColor = UIColor(rgb: 0x3da2f6, a: 1.0)



public let greenG40:UIColor = UIColor(rgb: 0x08ac62, a: 1.0)
public let greenG30:UIColor = UIColor(rgb: 0x09ca73, a: 1.0)
public let greenG20:UIColor = UIColor(rgb: 0x0bee87, a: 1.0)
public let greenG10:UIColor = UIColor(rgb: 0x29f599, a: 1.0)
public let greenG5:UIColor = UIColor(rgb: 0x52f7ad, a: 1.0)



public let blueInfoI400:UIColor = UIColor(rgb: 0x12a8c0, a: 1.0)
public let blueInfoI300:UIColor = UIColor(rgb: 0x15c6e2, a: 1.0)
public let blueInfoI200:UIColor = UIColor(rgb: 0x37d3ec, a: 1.0)
public let blueInfoI100:UIColor = UIColor(rgb: 0x5fdcf0, a: 1.0)
public let blueInfoI50:UIColor = UIColor(rgb: 0x8de6f4, a: 1.0)




public let lightGrayLG200:UIColor = UIColor(rgb: 0x3f5c6b, a: 1.0)
public let lightGrayLG150:UIColor = UIColor(rgb: 0x486a7b, a: 1.0)
public let lightGrayLG100:UIColor = UIColor(rgb: 0x537a8d, a: 1.0)
public let lightGrayLG50:UIColor = UIColor(rgb: 0x608ca1, a: 1.0)

public let almostBlack:UIColor = UIColor(rgb: 0x05090e, a: 1.0)
public let coolGrey:UIColor = UIColor(rgb: 0x8e98a2, a: 1.0)
public let darkGreyBlue:UIColor = UIColor(rgb: 0x37505d, a: 1.0)
public let darkGrey:UIColor = UIColor(rgb: 0x292b2c, a: 1.0)
public let charcoalGrey:UIColor = UIColor(rgb: 0x3b3d3e, a: 1.0)
public let charcoalGreyTwo:UIColor = UIColor(rgb: 0x424344, a: 1.0)

public let slateGrey:UIColor = UIColor(rgb: 0x656569, a: 1.0)
public let orangeyRed:UIColor = UIColor(rgb: 0xfa3434, a: 1.0)



public let zoriBlue:UIColor = UIColor(rgb: 0x1324c0, a: 1.0)
public let zoriPink:UIColor = UIColor(rgb: 0xb72ee9, a: 1.0)




public let skyBlue:UIColor = UIColor(rgb: 0x195CAA,a: 0.5)
public let solidBlue:UIColor = UIColor(rgb: 0x195CAA,a: 1.0)
public let solidGray:UIColor = UIColor(rgb: 0x9D9D9D, a: 1.0)
public let solidRed:UIColor = UIColor(rgb: 0xF44336, a: 1.0)
public let darkGray:UIColor = UIColor(rgb: 0646464, a: 1.0)
public let lightWhite:UIColor = UIColor(rgb: 0xF9F9F9, a: 1.0)
public let lightGray:UIColor = UIColor(rgb: 0xD3D3D3, a: 1.0)
