//
//  UIButton.swift
//
//  Created by Armin on 9/25/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func setBackgroundColor(_ color: UIColor, forState: UIControl.State) {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    
    func setTitleText(_ text: String, forState: UIControl.State) {
        self.setTitle(text, for: forState)
    }
    
    func setTitleColor(_ color: UIColor, forState: UIControl.State) {
        
        self.setTitleColor(color, for: forState)
        
    }
    
    func setImage(_ image: UIImage, forState: UIControl.State) {
        
        self.setImage(image, for: forState)
        
    }
    
    func setTitleFont(_ font: UIFont = UIFont()) {
        
        self.titleLabel?.font = font
        
    }
}
