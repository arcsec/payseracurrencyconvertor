//
//  NSString.swift
//
//  Created by Armin on 9/25/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit

extension Int
{
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int)
    {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}

//MARK: - String
extension String {
    
    
    
    var replaceArabicAndPersianLetters: String
       {

           let numbersDictionary : Dictionary = [ "ك" : "ک",
                                                  "دِ" : "د",
                                                  "بِ" : "ب",
                                                  "زِ" : "ز",
                                                  "ذِ" : "ذ",
                                                  "شِ" : "ش",
                                                  "سِ" : "س",
                                                  "ى" : "ی",
                                                  "ي" : "ی"]
        var str : String = self
               for (key,value) in numbersDictionary {
                   str =  str.replacingOccurrences(of: key, with: value)
               }
               return str
       }
    
//    'ك': 'ک',
//          'دِ': 'د',
//          'بِ': 'ب',
//          'زِ': 'ز',
//          'ذِ': 'ذ',
//          'شِ': 'ش',
//          'سِ': 'س',
//          'ى': 'ی',
//          'ي': 'ی'
    
    var replaceArabicAndPersianNumbers: String
    {

        let numbersDictionary : Dictionary = [ "۰" : "0","۱" : "1", "۲" : "2", "۳" : "3", "۴" : "4", "۵" : "5", "۶" : "6", "۷" : "7", "۸" : "8", "۹" : "9", "٤" : "4", "٥" : "5", "٦" : "6"]
            var str : String = self
            for (key,value) in numbersDictionary {
                str =  str.replacingOccurrences(of: key, with: value)
            }
            return str
    }
    
    var replaceEnNumbersToFA: String {
        
        let numbersDictionary : Dictionary = ["0" : "۰","1" : "۱", "2" : "۲", "3" : "۳", "4" : "۴", "5" : "۵", "6" : "۶", "7" : "۷", "8" : "۸", "9" : "۹"]
        var str : String = self
        for (key,value) in numbersDictionary {
            str =  str.replacingOccurrences(of: key, with: value)
        }
        
        return str
        
        
    }
    
    var replaceFANumbersToEN: String {
        
        
        let numbersDictionary : Dictionary = [ "۰" : "0","۱" : "1", "۲" : "2", "۳" : "3", "۴" : "4", "۵" : "5", "۶" : "6", "۷" : "7", "۸" : "8", "۹" : "9"]

        var str : String = self
        for (key,value) in numbersDictionary {
            str =  str.replacingOccurrences(of: key, with: value)
        }
        
        return str
        
        
    }
    
    var removeSpaces: String {
        
        var str:String = self
        if str.isEmpty == false
        {
            
            
            str = str.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")
            str = str.trimmingCharacters(in: .whitespaces)
            
            return str
        }
        else
        
        {
            return str
        }
        
    }
    
    public func nameOrLastNameIsValid()->Bool
    {
        let str:String = self
        
        let otpRegex = ".{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", otpRegex)
        return predicate.evaluate(with: str)
    }
    
    public func usernameIsValid()->Bool
    {
        let str:String = self
        
        let otpRegex = "[A-Za-z0-9.-_@]{6,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", otpRegex)
        return predicate.evaluate(with: str)
    }
    
    public func passwordIsValid()->Bool
    {
        let str:String = self
        
        let otpRegex = ".{6,16}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", otpRegex)
        return predicate.evaluate(with: str)
    }
    
    public func otpCodeIsValid()->Bool
    {
        let str:String = self
        if self.isDigits() == true
        {
            let otpRegex = "[0-9]{6}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", otpRegex)
            return predicate.evaluate(with: str)
        }
        
        else
        {
            return false
        }
        
        
    }
    
    
    public func shebaNumberIsValid()->Bool
    {
        let str:String = self
        
        if self.isDigits() == true
        {
            let shebaRegex = "[I][R][0-9]{24}|[0-9]{24}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", shebaRegex)
            return predicate.evaluate(with: str)
        }
        
        else
        {
            return false
        }
        
      
    }
    
    public func emailIsValid()->Bool
    {
        let str:String? = self
        
        if str != nil
        {
        
        let emailRegex = "^$|^[A-Za-z0-9.-_]{3,32}[@][A-Za-z0-9.-]{2,}\\.[A-Za-z]{2,}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return predicate.evaluate(with: str)
        }
        
        else
        {
            return false
        }
        
    }
    
    public func emailOrUserNameIsValid()->Bool
    {
        let str:String? = self
        
        if str != nil
        {
            
            let emailRegex = "^$|^[A-Za-z0-9.-_]{2,32}[@][A-Za-z0-9.-]{2,}\\.[A-Za-z]{2,}$"
            let userNameRegex = "^$|^[A-Za-z0-9 ,.'-]{6,64}$"
            
            let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
            let userNamePredicate = NSPredicate(format: "SELF MATCHES %@", userNameRegex)
            
            return emailPredicate.evaluate(with: str) || userNamePredicate.evaluate(with: str)
        }
        else
        {
            return false
        }
    }
    
    public func isPhone()->Bool
    {
        let str:String = self
        if self.isPhoneString() == true
        {
            let phoneRegex = "[0][0-9]{10}|[9][0-9]{10}|[0-9]{11}"
//            let phoneRegex = "[0-9]{10}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return predicate.evaluate(with: str)
        }
        
        else
        {
            return false
        }
        
    }
    
    private func isDigits()->Bool
    {
        guard self.count>0 else{return false}
        let numSet:Set<Character> = ["1","2","3","4","5","6","7","8","9","0"]
        return Set(self).isSubset(of: numSet)

    }
    
    private func isPhoneString()->Bool
    {
        guard self.count>0 else{return false}
        let numSet:Set<Character> = ["1","2","3","4","5","6","7","8","9","0", "+"]
        return Set(self).isSubset(of: numSet)
        
    }
    
    
    
}

extension String {
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: .utf8)
        else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

//MARK: - Number
extension Int {
    
    func currencyCommaRepresentation() -> String
    {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        //        numberFormatter.locale = NSLocale(localeIdentifier: "fa_IR") as Locale!
        
        return numberFormatter.string(from: NSNumber(value:self))!
    }
    
    func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

extension Int64 {
    
    func currencyCommaRepresentation64Bit() -> String
    {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        //        numberFormatter.locale = NSLocale(localeIdentifier: "fa_IR") as Locale!
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

