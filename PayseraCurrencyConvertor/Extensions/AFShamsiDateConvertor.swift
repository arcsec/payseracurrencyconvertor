//
//  AFShamsiDateConvertor.swift
//
//  Created by Armin on 4/23/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation


class AFShamsiDateConvertor: NSObject
{
    class func gregorianToPersian(dateString: String) -> String {
        let format = DateFormatter()
        format.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        format.dateFormat = "yyyy-MM-dd"
        let date = format.date(from: dateString)!
        let f = DateFormatter()
        let iranLocale = NSLocale(localeIdentifier: "fa_IR")
        f.locale = iranLocale as Locale?
        let persian = NSCalendar(identifier: NSCalendar.Identifier(rawValue: "persian"))
        f.calendar = persian as Calendar?
        f.dateStyle = .long
        let formattedDate = f.string(from: date)
        return formattedDate
    }
    
    class func persianToGregorian(dateString: String) -> String {
        let format = DateFormatter()
        format.locale = NSLocale(localeIdentifier: "fa_IR") as Locale?
        format.dateFormat = "yyyy-MM-dd"
        let date = format.date(from: dateString)!
        let f = DateFormatter()
        let iranLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        f.locale = iranLocale as Locale?
        let persian = NSCalendar(identifier: NSCalendar.Identifier(rawValue: "gregorian"))
        f.calendar = persian as Calendar?
        f.dateStyle = .long
        let formattedDate = f.string(from: date)
        return formattedDate
    }
}
