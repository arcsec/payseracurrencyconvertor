//
//  Date.swift
//
//  Created by Armin on 9/25/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation

extension String {
    
    func convertDateJalaliToString() -> String
    {
        
        let dateArray:[String] = self.components(separatedBy: " ")
        
        let year: String = dateArray[0]
        var month: String = dateArray[1]
        var day: String = dateArray[2]
        
        if day.first == "0" {
            
            day.remove(at: day.startIndex)
            
        }
        
        
        let monthInt:Int! = Int(month)
        
        switch monthInt {
        case 1:
            month = "فروردین"
            break
        case 2:
            month = "اردیبهشت"
            break
        case 3:
            month = "خرداد"
            break
        case 4:
            month = "تیر"
            break
        case 5:
            month = "مرداد"
            break
        case 6:
            month = "شهریور"
            break
        case 7:
            month = "مهر"
            break
        case 8:
            month = "آبان"
            break
        case 9:
            month = "آذر"
            break
        case 10:
            month = "دی"
            break
        case 11:
            month = "بهمن"
            break
        case 12:
            month = "اسفند"
            break
        default:
            month = ""
            break
        }
        
        let newDate:String = day+" "+month+" "+year
        
        return newDate
    }
    
    func convertJalaliStringToBackendReadable() -> String
    {
        
        let dateArray:[String] = self.components(separatedBy: " ")
        
        var day: String = dateArray[0].replaceFANumbersToEN
        var month: String = dateArray[1]
        let year: String = dateArray[2].replaceFANumbersToEN
        
        if day.first == "0" {
            
            day.remove(at: day.startIndex)
            
        }
        
        
        
        switch month {
        case "فروردین":
            month = "1"
            break
        case "اردیبهشت":
            month = "2"
            break
        case "خرداد":
            month = "3"
            break
        case "تیر":
            month = "4"
            break
        case "مرداد":
            month = "5"
            break
        case "شهریور":
            month = "6"
            break
        case "مهر":
            month = "7"
            break
        case "آبان":
            month = "8"
            break
        case "آذر":
            month = "9"
            break
        case "دی":
            month = "10"
            break
        case "بهمن":
            month = "11"
            break
        case "اسفند":
            month = "12"
            break
        default:
            month = ""
            break
        }
        
        let newDate:String = year+"-"+month+"-"+day
        
        return newDate
    }
    
}

//MARK: - NSDate
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date)) سال"   }
        if months(from: date)  > 0 { return "\(months(from: date)) ماه"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date)) هفته"   }
        if days(from: date)    > 0 { return "\(days(from: date)) روز"    }
        if hours(from: date)   > 0 { return "\(hours(from: date)) ساعت"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date)) دقیقه" }
        if seconds(from: date) > 0 { return "\(seconds(from: date)) ثانیه" }
        return ""
    }
}

