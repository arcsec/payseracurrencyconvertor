//
//  Localizable.swift
//
//  Created by Armin on 1/21/21.
//

import Foundation

extension String {

    var localized: String {
        
        let language = ""
        
        switch language {
        case "ar":
            switch self {
            

            default:
                return ""
            }
            
            
        
        case "en":
            
            switch self {
            
            case "eup_describtion":
                return "Phone number, username or email address"
                
            case "login":
                return "Login"
          
            case "signup":
                return "Sign Up"
                
            case "reset_password":
                return "Reset Password"
                
            case "verification_code":
                return "Verification Code"
                
            case "login_with":
                return "Log in with"
             
            case "forget_password":
                return "Forgot password?"
                
            case "dont_have_account":
                return "Don’t have an account? "
                
            case "create_account":
                return "Create Account"
                
            case "next":
                return "Next"
                
            case "password":
                return "Password"
                
            case "username_invalid":
                return "This username is not valid"
                
            case "phone_number":
                return "Phone Number"
                
            case "email_address":
                return "Email Address"
                
            case "have_account":
                return "Do you have an account?"
                
            case "create_account_description":
                return "Choose phone number or Email address and you can change it later"
                
            case "search_country_description":
                return "Search country name or code"
                
            case "select_country":
                return "Select your Country"
                
            case "username":
                return "Username"
                
            case "verification_code_description":
                return "Enter the confirmation code that we sent to "
                
            case "resend_code":
                return "Resend code"
                
                
            case "home":
                return "Home"
                
            case "notif":
                return "Notif"
                
            case "search":
                return "Search"
                
            case "profile":
                return "Profile"
                
            case "reset_password_description":
                return "Enter phone number or Email address or username and we’ll send verification code to change password "
                
            case "reset_password_destination_description":
                return "to receive the verification code, please select one of the following destinations"
                
            case "new_password":
                return "New Password"
                
            case "change_password":
                return "Change Password"
                
            case "confirm_password":
                return "Confirm Password"
                
            case "change_password_description":
                return "Choose a new password and keep it yourself"
                
            case "user_credential_description":
                return "Choose username and password, be aware you can change it later"
                
            case "fullname":
                return "Fullname"
                
            case "biography":
                return "Biography"
                
            case "mobile":
                return "Mobile"
                
            case "edit_profile":
                return "Edit Profile"
                
            case "add_people":
                return "Add People"

            case "messenger":
                return "Messenger"

            case "chats":
                return "Chats"
                
            case "channels":
                return "Channels"
                
            case "groups":
                return "Groups"
                
      
                
            default:
                return ""
            }
            
        default:
            return ""
        }
        
        
        
    }
    
}
