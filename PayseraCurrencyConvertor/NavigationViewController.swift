//
//  NavigationViewController.swift
//
//  Created by Armin on 10/1/20.
//  Copyright © 2020 Armin@Armin. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont.RobotoRegular.withSize(16) ,NSAttributedString.Key.foregroundColor: UIColor.white]
        
        self.navigationBar.barTintColor = .white
        self.navigationBar.isTranslucent = false
        
    }
    
}
