//
//  CurrencyExchangeTableViewCell.swift
//  PayseraCurrencyConvertor
//
//  Created by NGN on 4/15/22.
//

import UIKit
import SwiftyPlistManager
import SwiftyDrop

protocol CurrencyExchangeDelegate{
    func assetsAmountShouldUpdate()
}
class CurrencyExchangeTableViewCell: UITableViewCell {

    let dataPlistName = "DataStore"
    var delegate:CurrencyExchangeDelegate?
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var destinationExchangeView: ExchangeCell!
    @IBOutlet var sourceExchangeView: ExchangeCell!
    var sourceCurrencyPicker: UIPickerView = UIPickerView.init()
    var destinationCurrencyPicker: UIPickerView = UIPickerView.init()

    var pickerData:[String]!
    var selectedSourceCurrency:String?
    var selectedDestinationCurrency:String?
    var sourceAmount:String?
    var exchangeObj:Exchange?
    
    var parentViewController:UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.sourceAmount = "0.0"
        self.pickerData = Currencies.all.map {
            "\($0)"
        }
        
        self.submitButton.addCornerRadius(cornerSize: 15)
        self.sourceExchangeView.imageView.image = UIImage.init(systemName: "arrow.up")
        self.sourceExchangeView.imageView.backgroundColor = .red
        self.sourceExchangeView.actLabel.text = "Sell"
        self.sourceExchangeView.amountTextField.text = "0.0"
        self.sourceExchangeView.amountTextField.keyboardType = .numberPad
        self.sourceExchangeView.currencyTextField.text = self.pickerData[0]
        self.sourceExchangeView.amountTextField.delegate = self
        self.sourceExchangeView.amountTextField.addBorder(borderSize: 1, borderColor: grayG100)
        self.sourceExchangeView.currencyTextField.addBorder(borderSize: 1, borderColor: grayG100)
        self.sourceCurrencyPicker.delegate = self
        self.sourceCurrencyPicker.dataSource = self
        self.sourceExchangeView.currencyTextField.inputView = self.sourceCurrencyPicker
        
        
        
        self.destinationExchangeView.imageView.image = UIImage.init(systemName: "arrow.down")
        self.destinationExchangeView.imageView.backgroundColor = greenG40
        self.destinationExchangeView.actLabel.text = "Receive"
        self.destinationExchangeView.amountTextField.text = ""
        self.destinationExchangeView.amountTextField.isUserInteractionEnabled = false
        self.destinationExchangeView.amountTextField.textColor = greenG40
        self.destinationExchangeView.currencyTextField.text = self.pickerData[1]
        self.destinationExchangeView.amountTextField.delegate = self
        self.destinationExchangeView.amountTextField.addBorder(borderSize: 1, borderColor: grayG100)
        self.destinationExchangeView.currencyTextField.addBorder(borderSize: 1, borderColor: grayG100)
        self.destinationCurrencyPicker.delegate = self
        self.destinationCurrencyPicker.dataSource = self
        self.destinationExchangeView.currencyTextField.inputView = self.sourceCurrencyPicker
        
        self.submitButton.isUserInteractionEnabled = false
        self.submitButton.alpha = 0.5
        
    }
    @IBAction func currencyConvert(_ sender: Any) {
        self.exchangeCurrencies()
    }
    
    @IBAction func submitButton(_ sender: Any) {

        if self.sourceAmount == "" || self.selectedSourceCurrency == "" || self.selectedDestinationCurrency == "" {
            
            Drop.down("Please choose all inputs.", state: CustomDrop.failure)
            return
        }
        
        var exchnageFee:Double = 0.0
        
        let sourceAmountNumeric:Double = Double(self.sourceAmount ?? "0.0") ?? 0.0
        
        let commisionFee:Double = SwiftyPlistManager.shared.fetchValue(for: "Commision", fromPlistWithName: self.dataPlistName) as! Double
        
        let commisionTreshold:Double = SwiftyPlistManager.shared.fetchValue(for: "ZeroCommisionTreshold", fromPlistWithName: self.dataPlistName) as! Double

        let initialNonCommision:Int = SwiftyPlistManager.shared.fetchValue(for: "InitialNonCommision", fromPlistWithName: self.dataPlistName) as! Int

        let exchangeOrderNumber:Int = SwiftyPlistManager.shared.fetchValue(for: "ExchangeOrderNumber", fromPlistWithName: self.dataPlistName) as! Int
        
        var sourceCurrencyValue:Dictionary = SwiftyPlistManager.shared.fetchValue(for: selectedSourceCurrency ?? self.pickerData[0], fromPlistWithName: self.dataPlistName) as! Dictionary<String, AnyObject>
        
        var destinationCurrencyValue:Dictionary = SwiftyPlistManager.shared.fetchValue(for: selectedDestinationCurrency ?? self.pickerData[1], fromPlistWithName: self.dataPlistName) as! Dictionary<String, AnyObject>

        if initialNonCommision >= exchangeOrderNumber || sourceAmountNumeric <= commisionTreshold
        {
            exchnageFee = 0.0
        }
        else{
            exchnageFee = commisionFee
        }
        
        let currencyBalance:Double = sourceCurrencyValue["balance_amount"] as! Double
        let withdrowAmount:Double = sourceAmountNumeric + (sourceAmountNumeric * exchnageFee)

        if currencyBalance < withdrowAmount{
            
            Drop.down("Your balance is not sufficient.", state: CustomDrop.failure)
            return
        }
        
        
        let destinationCurrencyAmount:Double = destinationCurrencyValue["balance_amount"] as! Double

        let exchangeObjAmountNumeric:Double = Double(self.exchangeObj?.amount ?? "0.0") ?? 0.0
        
        let depositeAmount:Double = destinationCurrencyAmount + exchangeObjAmountNumeric
        
        sourceCurrencyValue["balance_amount"] = currencyBalance - withdrowAmount as AnyObject
        SwiftyPlistManager.shared.save(sourceCurrencyValue, forKey: selectedSourceCurrency ?? self.pickerData[0], toPlistWithName: dataPlistName) { (err) in}
        
        destinationCurrencyValue["balance_amount"] = depositeAmount as AnyObject
        SwiftyPlistManager.shared.save(destinationCurrencyValue, forKey: selectedDestinationCurrency ?? self.pickerData[1], toPlistWithName: dataPlistName) { (err) in}
        
        
        SwiftyPlistManager.shared.save(exchangeOrderNumber + 1, forKey: "ExchangeOrderNumber", toPlistWithName: dataPlistName) { (err) in}

        let msg:String = "You have convert \(self.sourceAmount ?? "0.0") \(self.selectedSourceCurrency ?? self.pickerData[0]) to \(self.exchangeObj?.amount ?? "") \(self.selectedDestinationCurrency ?? self.pickerData[1]). Commision fee -\(sourceAmountNumeric * exchnageFee) \(self.selectedSourceCurrency ?? self.pickerData[0])"

        let alert = UIAlertController(title: "Currency converted", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
        self.parentViewController?.present(alert, animated: true, completion: nil)
        
        self.delegate?.assetsAmountShouldUpdate()
        
    }
}

extension CurrencyExchangeTableViewCell{
    
    func exchangeCurrencies(){
        
        self.sourceAmount = self.sourceExchangeView.amountTextField.text
        
        let sourceAmountNumerical:CGFloat = CGFloat(Float(self.sourceAmount ?? "0.0") ?? 0.0)
        
        if sourceAmountNumerical <= 0 {
            self.sourceExchangeView.amountTextField.addBorder(borderSize: 1, borderColor: .red)
            return
        }
        else{
            BasicViewModel.shared.exchangeCurrencies(sourceCurrency: self.selectedSourceCurrency ?? self.pickerData[0], destinationCurrency: self.selectedDestinationCurrency ?? self.pickerData[1], amount: self.sourceAmount ?? "0.0") { success, obj in
                if success{
                    self.exchangeObj = obj as? Exchange
                    self.destinationExchangeView.amountTextField.text = self.exchangeObj?.amount
                    
                    self.submitButton.isUserInteractionEnabled = true
                    self.submitButton.alpha = 1.0
                }
            }
        }
    }
}

extension CurrencyExchangeTableViewCell:UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let str:String = self.pickerData[row]
        return str
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let title = String(row)
        let attributedString = NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor: charcoalGreyTwo])
        
        pickerView.backgroundColor = UIColor.gray
        
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel?
        if label == nil {
            label = UILabel()
        }
        
        let data = pickerData[row]
        let title = NSAttributedString(string: data , attributes: [NSAttributedString.Key.font: UIFont.RobotoRegular.withSize(16)])
        
        label?.attributedText = title
        label?.textAlignment = .center
        return label!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.sourceCurrencyPicker
        {
            let data = self.pickerData[row]
            self.sourceExchangeView.currencyTextField.text = data
            self.selectedSourceCurrency = data
            
        }
        else if pickerView == self.destinationCurrencyPicker
        {
            let data = self.pickerData[row]
            self.destinationExchangeView.currencyTextField.text = data
            self.selectedDestinationCurrency = data
            
        }
        
        else
        {
            return
        }
        
    }
}

extension CurrencyExchangeTableViewCell:UITextFieldDelegate
{
    // MARK: TextField-Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UserDefaults.standard.synchronize()
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.sourceExchangeView.amountTextField.addBorder(borderSize: 0, borderColor: .clear)

        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.sourceExchangeView.amountTextField.addBorder(borderSize: 0, borderColor: .clear)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
   
}
