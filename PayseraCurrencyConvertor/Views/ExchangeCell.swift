//
//  ExchangeCell.swift
//  PayseraCurrencyConvertor
//
//  Created by NGN on 4/15/22.
//

import UIKit

class ExchangeCell: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var currencyTextField: UITextField!
    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var actLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("ExchangeCell", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.imageView.makeCircular()
        
    }

}
