//
//  BasicViewModel.swift
//
//  Created by Armin on 3/4/22.
//

import UIKit

class BasicViewModel: NSObject {

    static let shared = BasicViewModel()
    
    func exchangeCurrencies(sourceCurrency:String, destinationCurrency:String, amount:String, completion: APICompletionHandler?)
    {
        Requests.shared.request(Router.exchange_currency(source_currency: sourceCurrency, destination_currency: destinationCurrency, amount: amount)) { (json) in
            
            let result = Exchange(json)
                completion!(true, result as Exchange? as AnyObject?)
        } onError: { (error) in
            completion!(false, error as AnyObject?)
            debugPrint(error)
        }

    }

}
    
   
